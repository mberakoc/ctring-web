import React from 'react'
import './SceneShifter.css'

class SceneShifter extends React.Component {

    render() {
        return (
            <div className="shifterContainer">
                <div className="upper shifter" onClick={ () => this.props.changeIndex(-1) }>
                    <i className="fas fa-chevron-up"></i>
                </div>
                <br/>
                <div className="lower shifter" onClick={ () => this.props.changeIndex(1) }>
                    <i className="fas fa-chevron-down"></i>
                </div>
            </div>
        )
    }
}

export default SceneShifter