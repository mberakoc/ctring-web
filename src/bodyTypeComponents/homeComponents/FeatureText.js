import React from 'react'
import './FeatureText.css'

const titles = ['Create strings easily', 'Use hundreds of awesome functions', 'A new way of handling strings', 'Convert String to char array anytime you need']
const texts = ['Ctring offers you the most concise way to create strings', 'Ctring has an astonishing amount of ubiqitous functions', 'With mutable functions, Ctring makes your development process flawless and robust',
'With conversion function, in Ctring convert String to char array in development process without any glitch']
const homeViewLimit = 4

class FeatureText extends React.Component {

    getRandomKey() {
        let randomKey = ''
        for (let i = 0; i < 16; ++i) randomKey += Math.floor(Math.random() * 10)
        return randomKey
    }

    render() {
        return (
            <div className="featureTextContainer">
                <div className="textTitle" key={ this.getRandomKey() } >
                    { titles[Math.abs(this.props.index % homeViewLimit)] }
                </div>
                <br />
                <div className="textBody" key={ this.getRandomKey() }>
                    { texts[Math.abs(this.props.index % homeViewLimit)] }
                </div>
            </div>
        )
    }
}

export default FeatureText