import React from 'react'
import './FeatureCanvas.css'

const codes = ['<b>#include "string.h"</b><br/><br/>int <b>main</b>(int argc, char *argv[])<br/>&#123;<br/>&emsp;String string = <b>_String</b>(<b>"Hello World!"</b>);<br/>&emsp;<b>print_string</b>(string);<br/>&emsp;return EXIT_SUCCESS;<br/>&#125;',
'<b>#include "string.h"</b><br/><br/>int <b>main</b>(int argc, char *argv[])<br/>&#123;<br/>&emsp;String string = <b>_String</b>(<b>"i have the upperhand."</b>);<br/>&emsp;<b>map</b>(string, __MapToUppercase);<br/>&emsp;<b>print_string</b>(string);<br/>&emsp;return EXIT_SUCCESS;<br/>&#125;',
'<b>#include "string.h"</b><br/><br/>int <b>main</b>(int argc, char *argv[])<br/>&#123;<br/>&emsp;String string = <b>_String</b>(<b>".thgir denrut ma I woN"</b>);<br/>&emsp;<b>reverse</b>(string);<br/>&emsp;<b>print_string</b>(string);<br/>&emsp;return EXIT_SUCCESS;<br/>&#125;',
'<b>#include "string.h"</b><br/><br/>int <b>main</b>(int argc, char *argv[])<br/>&#123;<br/>&emsp;String string = <b>_String</b>(<b>"I am now a normal char array."</b>);<br/>&emsp;char * char_array = <b>convert_to_char_array</b>(string);<br/>&emsp;<b>puts</b>(char_array);<br/>&emsp;return EXIT_SUCCESS;<br/>&#125;']
const outputs = ['<b>>> Hello World!</b>', '<b>>> I HAVE THE UPPERHAND.</b>', '<b>>> Now I am turned right.</b>', '<b>>> I am now a normal char array.</b>']
const homeViewLimit = 4

export default class FeatureCanvas extends React.Component {

    render() {
        return (
            <div className="canvasContainer">
                <div className="codeArea">
                    <div className="code" dangerouslySetInnerHTML={{ __html: codes[Math.abs(this.props.index % homeViewLimit)] }}></div>
                    <div className="output" dangerouslySetInnerHTML={{ __html: outputs[Math.abs(this.props.index % homeViewLimit)] }}></div>
                </div>
            </div>
        )
    }
}