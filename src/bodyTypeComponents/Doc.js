import React from 'react'
import './Doc.css'
import './Common.css'

class Doc extends React.Component {

    constructor(props) {
        super(props)
        this.state =  {
            index: 0,
        }
    }

    render() {
        return (
            <div className="docContainer">
                <div className="docContext"> { functionFragments[this.state.index] } </div>
                <div className="docNavigation">
                    <span onClick={ () =>  this.setState({ index: 0 })}>_String</span>
                    <span onClick={ () => this.setState({ index: 1 })}>append_char_unit</span>
                    <span onClick={ () => this.setState({ index: 2 })}>length</span>
                    <span onClick={ () => this.setState({ index: 3 })}>index_of</span>
                    <span onClick={ () => this.setState({ index: 4 })}>char_at</span>
                    <span onClick={ () => this.setState({ index: 5 })}>reverse</span>
                    <span onClick={ () => this.setState({ index: 6 })}>print_string</span>
                    <span onClick={ () => this.setState({ index: 7 })}>convert_to_char_array</span>
                    <span onClick={ () => this.setState({ index: 8 })}>copy</span>
                    <span onClick={ () => this.setState({ index: 9 })}>__print_string</span>
                    <span onClick={ () => this.setState({ index: 10 })}>insert</span>
                    <span onClick={ () => this.setState({ index: 11 })}>append</span>
                    <span onClick={ () => this.setState({ index: 12 })}>prepend</span>
                    <span onClick={ () => this.setState({ index: 13 })}>__MapToUpperCase</span>
                    <span onClick={ () => this.setState({ index: 14 })}>__MapToLowerCase</span>
                    <span onClick={ () => this.setState({ index: 15 })}>map</span>
                    <span onClick={ () => this.setState({ index: 16 })}>shrink</span>
                    <span onClick={ () => this.setState({ index: 17 })}>substring</span>
                </div>
                <div className="navButtons">
                    <div className="upper shifter">
                        <i className="fas fa-chevron-up"></i>
                    </div>
                    <br/>
                    <div className="lower shifter">
                        <i className="fas fa-chevron-down"></i>
                    </div>
                </div>
            </div>
        )
    }
}

class String extends React.Component {

    render() {
        return (
            <React.Fragment>
                    <div className="title">_String</div>
                    <div className="functionNode">String <span className="bold">_String</span>(char *char_array)</div>
                    <div className="paragraph">Creates a String structure for given char array. _String is in the core of the library since it is the main mechanism to
                    create new strings. Almost every function depends on it explicitly or implicitly.</div>
            </React.Fragment>
        )
    }
}

class AppendCharUnit extends React.Component {

    render() {
        return (
            <React.Fragment>
                    <div className="title">append_char_unit</div>
                    <div className="functionNode">void <span className="bold">append_char_unit</span>(CharListPtr *char_list_header_ptr, CharListPtr *char_list_tail_ptr, char char_unit)</div>
                    <div className="paragraph">Appends given char unit to the CharList. This function is the only dependence for _String. Hence it is a crucial yet low-level function. Not 
                    recommended to use this function in your development process. Not because function is prone to errors, but because you probably never need this function.</div>
            </React.Fragment>
        )
    }
}

class Length extends React.Component {

    render() {
        return (
            <React.Fragment>
                    <div className="title">length</div>
                    <div className="functionNode">unsigned <span className="bold">length</span>(String string)</div>
                    <div className="paragraph">Returns the length of a given string. In a function invoke it once and put the value in variable since invoking it over and over again would be cumbersome.</div>
            </React.Fragment>
        )
    }
}

class IndexOf extends React.Component {

    render() {
        return (
            <React.Fragment>
                    <div className="title">index_of</div>
                    <div className="functionNode">int <span className="bold">index_of</span>(String string, String key)</div>
                    <div className="paragraph">Returns the first index of a key in given string. If key is not in string then it will return -1.</div>
            </React.Fragment>
        )
    }
}

class CharAt extends React.Component {

    render() {
        return (
            <React.Fragment>
                    <div className="title">char_at</div>
                    <div className="functionNode">char <span className="bold">char_at</span>(String string, unsigned index)</div>
                    <div className="paragraph">Returns the char at given index. If index is greater then string's length or less then zero. Program will end printing an error message.</div>
            </React.Fragment>
        )
    }
}

class Reverse extends React.Component {

    render() {
        return (
            <React.Fragment>
                    <div className="title">reverse</div>
                    <div className="functionNode">void <span className="bold">reverse</span>(String string)</div>
                    <div className="paragraph">Reverses the given string.</div>
            </React.Fragment>
        )
    }
}


class PrintString extends React.Component {

    render() {
        return (
            <React.Fragment>
                    <div className="title">print_string</div>
                    <div className="functionNode">void <span className="bold">print_string</span>(String string)</div>
                    <div className="paragraph">Prints a string.</div>
            </React.Fragment>
        )
    }
}

class ConvertToCharArray extends React.Component {

    render() {
        return (
            <React.Fragment>
                    <div className="title">convert_to_char_array</div>
                    <div className="functionNode">char * <span className="bold">convert_to_char_array</span>(String string)</div>
                    <div className="paragraph">Converts the given string to char array. This function is very useful when you need classic notation for strings in formatting or 
                    as a function argument.</div>
            </React.Fragment>
        )
    }
}

class Copy extends React.Component {

    render() {
        return (
            <React.Fragment>
                    <div className="title">copy</div>
                    <div className="functionNode">String <span className="bold">copy</span>(String string)</div>
                    <div className="paragraph">Copies a string and returns the new string. The process is deep copying. Function will not copy reference of given string. Instead it will
                    create a new string and initiliases it using given string.</div>
            </React.Fragment>
        )
    }
}

class DPrintString extends React.Component {

    render() {
        return (
            <React.Fragment>
                    <div className="title">__print_string</div>
                    <div className="functionNode">void <span className="bold">__print_string</span>(String string)</div>
                    <div className="paragraph">Prints the given string in a detailed way. Shows address and character value of each string cell. Can be used for debugging.</div>
            </React.Fragment>
        )
    }
}

class Insert extends React.Component {

    render() {
        return (
            <React.Fragment>
                    <div className="title">insert</div>
                    <div className="functionNode">void <span className="bold">insert</span>(StringPtr core_ptr, String node, int index)</div>
                    <div className="paragraph">Inserts the node into core string at given index. Giving an index which is out of core string will end up with error.</div>
            </React.Fragment>
        )
    }
}

class Append extends React.Component {

    render() {
        return (
            <React.Fragment>
                    <div className="title">append</div>
                    <div className="functionNode">void <span className="bold">append</span>(StringPtr core_ptr, String node)</div>
                    <div className="paragraph">Inserts the node into end of the core. Giving an index which is out of core string will end up with error.</div>
            </React.Fragment>
        )
    }
}

class Prepend extends React.Component {

    render() {
        return (
            <React.Fragment>
                    <div className="title">prepend</div>
                    <div className="functionNode">void <span className="bold">prepend</span>(StringPtr core_ptr, String node)</div>
                    <div className="paragraph">Inserts the node into beginning of the core. Giving an index which is out of core string will end up with error.</div>
            </React.Fragment>
        )
    }
}

class MapToUpperCase extends React.Component {

    render() {
        return (
            <React.Fragment>
                    <div className="title">__MapToUpperCase</div>
                    <div className="functionNode">char <span className="bold">__MapToUpperCase</span>(char input)</div>
                    <div className="paragraph">The default mapper function. Converts an alphabetical character to upper case.</div>
            </React.Fragment>
        )
    }
}

class MapToLowerCase extends React.Component {

    render() {
        return (
            <React.Fragment>
                    <div className="title">__MapToLowerCase</div>
                    <div className="functionNode">char <span className="bold">__MapToLowerCase</span>(char input)</div>
                    <div className="paragraph">The default mapper function. Converts an alphabetical character to lower case.</div>
            </React.Fragment>
        )
    }
}

class Map extends React.Component {

    render() {
        return (
            <React.Fragment>
                    <div className="title">map</div>
                    <div className="functionNode">void <span className="bold">map</span>(String string, char (__Map)(char))</div>
                    <div className="paragraph">One of the must powerful functions of the library. Takes a string and a mapper function. In library there are two default mapper functions.
                    You can add new mapper functions as much as you want. Using mapper function maps the given string.</div>
            </React.Fragment>
        )
    }
}

class Shrink extends React.Component {

    render() {
        return (
            <React.Fragment>
                    <div className="title">shrink</div>
                    <div className="functionNode">void <span className="bold">shrink</span>(StringPtr string_ptr, int begin_index, int end_index)</div>
                    <div className="paragraph">Cuts the given string by the given begin and end index. End index is exclusive and begin is inclusive.</div>
            </React.Fragment>
        )
    }
}

class Substring extends React.Component {

    render() {
        return (
            <React.Fragment>
                    <div className="title">substring</div>
                    <div className="functionNode">void <span className="bold">substring</span>(StringPtr string_ptr, int begin_index, int end_index)</div>
                    <div className="paragraph">Returns the substring created by given indeces. End index is exclusive and begin is inclusive.</div>
            </React.Fragment>
        )
    }
}

const functionFragments = [<String />, <AppendCharUnit />, <Length />, <IndexOf />, <CharAt />, <Reverse />, <PrintString />, <ConvertToCharArray />,
    <Copy />, <DPrintString />, <Insert />, <Append />, <Prepend />, <MapToUpperCase />, <MapToLowerCase />, <Map />, <Shrink />, <Substring />]

export default Doc