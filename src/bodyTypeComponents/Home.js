import React from 'react'
import './Home.css'
import SceneShifter from './homeComponents/SceneShifter'
import FeatureText from './homeComponents/FeatureText'
import FeatureCanvas from './homeComponents/FeatureCanvas'

class Home extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            index: 0,
        }
        this.changeIndex = this.changeIndex.bind(this)
    }

    changeIndex(indexAdder) {
        let currentIndex = this.state.index + indexAdder
        this.setState({
            index: currentIndex,
        })
    }

    componentDidMount() {
        //setInterval(() => this.changeIndex(1), 5000)
    }

    render() {
        return (
            <div className="homeContainer">
                <SceneShifter changeIndex={ this.changeIndex }/>
                <FeatureText index={ this.state.index }/>
                <FeatureCanvas index={ this.state.index }/>
            </div>
        )
    }
}

export default Home