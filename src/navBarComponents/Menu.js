import React from 'react'
import './Menu.css'

class Menu extends React.Component {

    render() {
        return (
            <div className="responsiveLayer">
                <div className="hamburgerIcon"><i className="fas fa-bars"></i></div>
                <div className="menuContainer">
                    <div onClick={ () => this.props.sendCurrentBodyType("Home") }>Home</div>
                    <div onClick={ () => this.props.sendCurrentBodyType("Doc") }>Documentation</div>
                    <div onClick={ () => this.props.sendCurrentBodyType("Tutorial") }>Tutorial</div>
                    <div onClick={ () => this.props.sendCurrentBodyType("Contribute") }>Contribute</div>
                    <div onClick={ () => this.props.sendCurrentBodyType("Note") }>Patch Notes</div>
                </div>
            </div>
        )
    }

}

export default Menu