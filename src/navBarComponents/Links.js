import React from 'react'
import './Links.css'

class Links extends React.Component {

    render() {
        return (
            <div className="linksContainer">
                <div title="Gitlab Link" onClick={ () => window.open('https://gitlab.com/mberakoc/ctring', '_blank')}><i className="fas fa-code-branch"></i></div>
                <div title="Star Project" onClick={ () => window.open('https://medium.com/@z.bera97', '_blank')}><i className="fas fa-heart"></i></div>
                <button className="downloadButton" target="_blank" rel="noopener noreferrer" onClick={() =>  window.open('http://www.mediafire.com/file/24zxio83teutlmi/ctring.h/file', '_blank') }>DOWNLOAD</button>
            </div>
        )
    }
}

export default Links