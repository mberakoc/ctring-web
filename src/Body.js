import React from 'react'
import './Body.css'
import Home from './bodyTypeComponents/Home'
import Doc from './bodyTypeComponents/Doc'
import Tutorial from './bodyTypeComponents/Tutorial'
import Contribute from './bodyTypeComponents/Contribute'
import Note from './bodyTypeComponents/Note'

class Body extends React.Component {

    renderSwitch(currentBodyType) {
        switch(currentBodyType) {
            case 'Home': return <Home />
            case 'Doc': return <Doc />
            case 'Tutorial': return <Tutorial />
            case 'Contribute': return <Contribute />
            case 'Note': return <Note />
            default: return null
        }
    }

    render() {
        let currentBodyType = this.props.bodyType
        return (
            <div className="componentAdjuster">
                {this.renderSwitch(currentBodyType)}
            </div>
        )
    }
}

export default Body