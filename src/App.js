import React from 'react';
import './App.css';
import Header from './Header'
import Body from './Body'
class App extends React.Component {

  constructor(props) {
    super(props)
    this.state = {
      currentBodyType: 'Home'
    }
    this.getCurrentBodyType = this.getCurrentBodyType.bind(this)
  }

  getCurrentBodyType(newBodyType) {
    this.setState({
      currentBodyType: newBodyType,
    })
  }

  render() {
    return (
      <div className="App">
        <Header sendCurrentBodyType={ this.getCurrentBodyType }/>
        <Body bodyType={ this.state.currentBodyType }/>
      </div>
    )
  }
}

export default App;
