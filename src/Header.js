import React from 'react';
import "./Header.css"
import ProductLogo from './navBarComponents/Product'
import Menu from './navBarComponents/Menu'
import Links from './navBarComponents/Links'

class Header extends React.Component {

    render() {
        return (
            <div className="headerContainer">
                <div className="productLogo">
                    <ProductLogo />
                </div>
                <div className="menu">
                    <Menu sendCurrentBodyType={ this.props.sendCurrentBodyType }/>
                </div>
                <div className="links">
                    <Links />
                </div>
            </div>
        )
    }
}

export default Header;